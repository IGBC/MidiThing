#include <cstddef>
#include <cstdint>
#include <functional>
#include <map>
#include <memory>

#include "midi-pattern.h"
/// Voice Messages
constexpr uint8_t NOTE_OFF       = 0b10000000;
constexpr uint8_t NOTE_ON        = 0b10010000;
constexpr uint8_t POLY_EXPR      = 0b10100000;
constexpr uint8_t CONTROL_CHANGE = 0b10110000;
constexpr uint8_t PROGRAM_CHANGE = 0b11000000;
constexpr uint8_t AFTERTOUCH     = 0b11010000;
constexpr uint8_t PITCH_BEND     = 0b11100000;
// Sysex Messages
constexpr uint8_t SYSEX_START    = 0b11110000;
constexpr uint8_t SYSEX_END      = 0b11110111;
// MTC Messages
constexpr uint8_t MTC_QFRAME     = 0b11110001;
constexpr uint8_t MTC_POSITION   = 0b11110010;
constexpr uint8_t MTC_SONG_SEL   = 0b11110011;
// MMC and Midi clock Messages
constexpr uint8_t MIDI_CLOCK     = 0b11111000;
constexpr uint8_t MMC_START      = 0b11111010;
constexpr uint8_t MMC_CONTINUE   = 0b11111011;
constexpr uint8_t MMC_STOP       = 0b11111100;



/// Pure virtual implementation of a MIDI port
/// For hiding the gritty details of getting the
/// various types of port on the real hardware
class MidiPort {
public:
    /// Called to write a byte the port 
    /// returns false if the buffer is full
    /// always check this.
    virtual inline bool write(uint8_t*, size_t) = 0;

    /// Called to check if the buffer is empty yet
    /// Typically used at the start of a tick to see
    /// if the port overran
    virtual bool empty(uint8_t) = 0;
};


class RetirementQueue {
private:
    std::function<void(uint8_t)> on_retire;
    std::map<uint8_t, size_t> notes;
public:
    RetirementQueue(std::function<void(uint8_t)> on_retire) : on_retire(on_retire) {};
    
    // Copying this would create multiple note off events so don't
    RetirementQueue(RetirementQueue&) = delete;
    RetirementQueue& operator=(RetirementQueue&) = delete;

    void push(uint8_t note, size_t len) {
        notes.insert_or_assign(note, len);
    }

    void tick() {
        for (auto kv = notes.begin(); kv != notes.end(); kv++) {
            if (--(kv->second) == 0) {
                on_retire(kv->first);
                notes.erase(kv);
            }  
        }
    }

    /// Called when the midi port is issued a panic (all notes off now)
    /// this function does not actually issue the panic, just syncs state
    void panic() {
        notes.clear();
    }
    
};

class Sequencer {
private:
    size_t step;
    uint8_t channel;

    std::shared_ptr<MidiPattern> pattern;
    std::unique_ptr<MidiPattern::iterator> loc;

    RetirementQueue retirement_queue;
    std::shared_ptr<MidiPort> port;

    inline void blocking_send(uint8_t*, size_t);
    
    void retire_note(uint8_t);

public: 
    void tick();

    void seek(uint8_t);

    bool empty();
};

inline void Sequencer::blocking_send(uint8_t *bytes, size_t len) {
    bool sent;
    do 
        sent = port->write(bytes, len);
    while(!sent);
    
}

void Sequencer::retire_note(uint8_t note) {
    uint8_t word[2];
    word[0] =  NOTE_OFF | channel;
    word[1] = note;
    blocking_send(word, sizeof(word));
}

void Sequencer::tick() {
    //todo stuff
    
}