#include "button-driver.h"
#include <array>
#include <wiringPi.h>

// { Down, Up, Left, Right, A, B } In that order
constexpr std::array<int, numButtons> GPIONUMBERS = {22, 17, 27, 23, 5, 6};

ButtonDriver::ButtonDriver() {
    wiringPiSetupGpio();
    for (int i : GPIONUMBERS) {
       pinMode(i, INPUT);
       pullUpDnControl(i, PUD_UP);

    }
}

ButtonDriver::~ButtonDriver() {
    for (int i : GPIONUMBERS) {
       pinMode(i, INPUT);
       pullUpDnControl(i, PUD_OFF);
    }
}

bool ButtonDriver::get_button(Button b) {
    return !((bool)digitalRead(GPIONUMBERS[b]));
}