#pragma once

#include "button-driver.h"
#include <vector>
#include <memory>

enum Event {
    UpPressed = 0, 
    DownPressed, 
    LeftPressed,
    RightPressed,
    APressed,
    BPressed,
};

template<typename D> class Scene {
public:
    virtual void draw(D&) = 0;
    virtual bool event(Event) = 0;
};

template<typename D> class UI {
private:
    std::vector<std::shared_ptr<Scene<D>>> m_scene_stack;
    ButtonDriver buttons;
    D display;

public:
    UI(D&& display): display(display) {};


    void push_scene(std::shared_ptr<Scene<D>> s) {
        m_scene_stack.push_back(s);
    }

    void run() {
        while (true) {
            if (m_scene_stack.empty())
                break;
            
            m_scene_stack.back()->draw(display);

            if (buttons.get_button(Button::A)) {
                bool r = m_scene_stack.back()->event(Event::APressed);
                if (!r) m_scene_stack.pop_back();
            }
            if (buttons.get_button(Button::B)) {
                bool r = m_scene_stack.back()->event(Event::BPressed);
                if (!r) m_scene_stack.pop_back();
            }
            if (buttons.get_button(Button::Up)) {
                bool r = m_scene_stack.back()->event(Event::UpPressed);
                if (!r) m_scene_stack.pop_back();
            }
            if (buttons.get_button(Button::Down)) {
                bool r = m_scene_stack.back()->event(Event::DownPressed);
                if (!r) m_scene_stack.pop_back();
            }
            if (buttons.get_button(Button::Left)) {
                bool r = m_scene_stack.back()->event(Event::LeftPressed);
                if (!r) m_scene_stack.pop_back();
            }
            if (buttons.get_button(Button::Right)) {
                bool r = m_scene_stack.back()->event(Event::RightPressed);
                if (!r) m_scene_stack.pop_back();
            }  

            lcd_delay(16);          
        }

        display.clear();
        display.end();
    }
};

