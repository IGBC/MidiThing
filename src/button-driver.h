#pragma once
#include <cstddef>

constexpr size_t numButtons = 6;

enum Button {Down = 0, Up, Left, Right, A, B,};

class ButtonDriver {
public:
    ButtonDriver();
    ~ButtonDriver();

    ButtonDriver(const ButtonDriver&) = delete;
    ButtonDriver& operator=(const ButtonDriver&) = delete;

    bool get_button(Button);
};
