#pragma once
// lcdgfx modules must be loaded before stl code due to a namespacing problem
#include "lcdgfx.h"

#include "midi_ui.h"
#include <vector>
#include <functional>
#include <string>
#include <iostream>

template<typename D> class Menu : public Scene<D> {
public:
    Menu() : m_selected(0), m_continue(true) {};
    ~Menu() = default;
    void add_menu_item(std::string name, std::function<void()> callback) {
        m_callbacks.emplace_back(name, callback);
    }

    void draw(D& display) {
        //display.clear();
        display.setFixedFont( ssd1306xled_font6x8 );
    
        for (size_t i = 0; i < m_callbacks.size(); i++ ) {
            Callback &cb = m_callbacks[i];
            std::string &item = std::get<0>(cb);
            if ( i == m_selected )
                display.invertColors();
            display.printFixed(0, (i + 1)*8, item.c_str(), STYLE_NORMAL);
            if ( i == m_selected )
                display.invertColors();
        }

        display.printFixed(60, 32, std::to_string(m_selected).c_str(), STYLE_NORMAL);
    }

    bool event(Event e) {
        switch (e) {
        case Event::UpPressed: 
            if (m_selected > 0)
                m_selected--;
            break;
        case Event::DownPressed: 
            if (m_selected < m_callbacks.size() - 1)
                m_selected++;
            break;
        case Event::APressed:
            auto cb = m_callbacks[m_selected];
            auto f = std::get<1>(cb);
            f();
            break;
        }

        return m_continue;
    }

    void terminate() {
        m_continue = false;
    }



private:
    using Callback = std::pair<std::string, std::function<void()>>;
    std::vector<Callback> m_callbacks;
    size_t m_selected;
    bool m_continue;
};