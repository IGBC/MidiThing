#include "midi-pattern.h"

#include <algorithm>
#include <cassert>

MidiPattern::MidiPattern(size_t length, MidiInterface interface, uint8_t channel, uint8_t program, size_t ticks) : length(length), interface(interface) {
    assert(ticks > 0);
    this->ticks = ticks;
    assert(program < 128);
    this->program = program;
    assert(channel < 16);
    this->channel = channel;
}

MidiPattern::MidiPattern(size_t length, MidiInterface interface, uint8_t channel, uint8_t program) : 
    MidiPattern(length, interface, channel, program, 6) {} // 1 step is 1/16th


bool sortMidiEvents(MidiEvent i, MidiEvent j) {
    size_t order_i = (i.step << 8) + i.substep;
    size_t order_j = (j.step << 8) + i.substep;
    return order_i < order_j;
}

inline size_t MidiPattern::get_length_in_ticks() {
    return length * ticks;
}

inline size_t MidiPattern::get_step_length() {
    return ticks;
} 

void MidiPattern::add_event(MidiEvent&& event) {
    if (event.step > length) {
        length = event.step;
    }
    events.push_back(event);
    std::sort(events.begin(), events.end(), sortMidiEvents);
}


std::vector<MidiEvent>::iterator MidiPattern::begin() {
    return events.begin();
}

std::vector<MidiEvent>::iterator MidiPattern::end() {
    return events.end();
}


inline void MidiPattern::set_length(size_t s) {
    length = s;
}
inline void MidiPattern::set_step_length(size_t s) {
    ticks = s;
}
inline void MidiPattern::set_interface(MidiInterface s) {
    interface = s;
}
inline void MidiPattern::set_channel(uint8_t s) {
    assert(s < 16);
    channel = s;
}
inline void MidiPattern::set_program(uint8_t s) {
    assert(s < 128);
    program = s;
}