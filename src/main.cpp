#include <lcdgfx.h>
#include <lcdgfx_gui.h>
#include <iostream>

#include "button-driver.h"
#include "midi_menu.h"

using std::cout;
using std::endl;


const char *menuItems[] =
{
    "Option 1",
    "Option 2",
    "Option 3",
    "Option 4",
    "quit",
};


int main() {
    DisplaySSD1306_128x64_I2C display(-1);
    display.begin();
    
    cout << "Hello World" << endl << endl;

    //LcdGfxMenu menu( menuItems, sizeof(menuItems) / sizeof(char *) );
    display.setFixedFont( ssd1306xled_font6x8 );
    display.clear();
    // menu.show(display);

    bool run(true);

    auto m = std::make_shared<Menu<DisplaySSD1306_128x64_I2C>>();
    m->add_menu_item("nothing 1", [](){ cout << "nothing happened" << endl; });
    m->add_menu_item("nothing 2", [](){ cout << "nothing happened 2" << endl; });
    m->add_menu_item("nothing 3", [](){ cout << "nothing happened 3" << endl; });
    m->add_menu_item("nothing 4", [](){ cout << "nothing happened 4" << endl; });
    m->add_menu_item("Quit", [m](){ m->terminate();});

    UI<DisplaySSD1306_128x64_I2C> ui(std::move(display));
    ui.push_scene(m);

    ui.run();   
}
