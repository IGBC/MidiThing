#pragma once

#include <cstddef>
#include <cstdint>
#include <variant>
#include <vector>
#include <string>

enum MidiInterface { Midi1, Midi2, USB, BT };

struct MidiEventNote {
    uint8_t note;
    uint8_t velocity;
    size_t length;
}; 

struct MidiEventCC {
    uint8_t CC;
};

struct MidiEventSysExStr {
    std::string str;
};

struct MidiEventExecScript {
    //TODO: pointer to script;
};

struct MidiEvent {
    // timestep that the event occurs on
    size_t step;
    // priority list for determining the order in which events are processed
    uint8_t substep;

    // Midi data
    std::variant<MidiEventNote, MidiEventCC, MidiEventSysExStr, MidiEventExecScript> event;
};

class MidiPattern {
private:
    // The number of steps in the pattern before it can end or loop
    size_t length;
    // The length of a step in ticks (24ppq)
    size_t ticks;
    // The interface the patthern outputs on (interpreted by engine)
    MidiInterface interface;
    // The channel number of this pattern
    uint8_t channel;
    // The patch number used for this pattern
    uint8_t program;
    // The steps
    std::vector<MidiEvent> events;

public:
    MidiPattern(size_t length, MidiInterface interface, uint8_t channel, uint8_t program, size_t ticks);
    MidiPattern(size_t length, MidiInterface interface, uint8_t channel, uint8_t program);

    using iterator = std::vector<MidiEvent>::iterator;

    inline size_t get_length_in_ticks();
    inline size_t get_step_length();
    void add_event(MidiEvent&&);
    //std::vector<MidiEvent> get_events(size_t step);

    iterator begin();
    iterator end();

    inline void set_length(size_t);
    inline void set_step_length(size_t);
    inline void set_interface(MidiInterface);
    inline void set_channel(uint8_t);
    inline void set_program(uint8_t);
};